# README #
- This project allows any number of users to store albums full of pictures. 



# HOW TO START #
- After running the Java file (src/view/PhotoAlbum.java), you will see a login page. In the username field, enter "admin" to log in as the administrator. Now, you can create/delete user accounts, who you can then proceed to log
- in as. The rest is mostly self explanatory except for one thing: When adding photos, remember to put in the ADDRESS of the photo, not the name. (e.g. enter "C:\Users\Alice\Downloads\photo.jpeg", and not just "photo.jpeg").




### Contributors ###
- Manan Doshi
- Mehul Doshi
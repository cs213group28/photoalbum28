package model;

import java.util.ArrayList;

public class User {
	private String username;
	public ArrayList<Album> albums; // This user's albums
	
	/**
	 * Constructor
	 * @param username
	 */
	public User(String username){
		this.username = username;
		albums = new ArrayList<Album>();
	}
	
	/**
	 * Getter for username
	 * @return this User's username
	 */
	public String getName(){
		return this.username;
	}
	
	/**
	 * Adds an album to this user's list
	 * @param a - album to be added
	 */
	public void addAlbum(Album a){
		albums.add(a);
	}
	
	/**
	 * Delete an album
	 * @param a - album to be deleted
	 */
	public void deleteAlbum(Album a){
		albums.remove(a);
	}
	
	/**
	 * Delete an album
	 * @param albumName - name of album to be deleted
	 */
	public void deleteAlbum(String albumName){
		for(int i = 0; i < albums.size(); i++){
			if(albums.get(i).getName().equals(albumName)){
				albums.remove(i);
				return;
			}
		}
		// If made it here, then no album exists with name albumName
	}
	
	/**
	 * Getter
	 * @return this.albums
	 */
	public ArrayList<Album> getAlbums(){
		return this.albums;
	}
	
	/**
	 * toString, needed for listView
	 */
	public String toString(){
		return this.username;
	}
}

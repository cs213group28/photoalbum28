package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Photo implements Serializable{
	private String photoName;
	private String address;
	private String caption;
	private ArrayList<String> tags; // Stores tags
	private Calendar dateTaken; // Need for date
	private static final long serialVersionUID = 1;
	
	/**
	 * Constructor
	 * @param photoName - name of file
	 * @param address - full address
	 */
	public Photo(String photoName, String address){
		this.photoName = photoName;
		this.address = address;
		tags = new ArrayList<String>();
		dateTaken = Calendar.getInstance();
	}
	
	/**
	 * Getter
	 * @return this.photoName
	 */
	public String getName(){
		return this.photoName;
	}
	
	/**
	 * Getter
	 * @return this.address
	 */
	public String getAddress(){
		return this.address;
	}
	
	/**
	 * Getter
	 * @return this.tags
	 */
	public ArrayList<String> getTags(){
		return this.tags;
	}
	
	
	/**
	 * Adds a tag
	 * @param t - tag to be added
	 */
	public void addTag(String t){
		this.tags.add(t);
	}
	
	/**
	 * Deletes a tag
	 * @param t - tag to be deleted
	 */
	public void deleteTag(String t){
		this.tags.remove(t);
	}
	
	/**
	 * Checks if has this tag
	 * @param s - tag we are searching for
	 * @return True if does have this tag, false otherwise
	 */
	public boolean hasTag(String s){
		boolean temp = false;
		
		for(int i = 0; i < tags.size(); i++){
			if(tags.get(i).equals(s)) temp = true;
		}
		
		return temp;
	}
	
	
	/**
	 * Getter
	 * @return this.caption
	 */
	public String getCaption(){
		return this.caption;
	}
	
	/**
	 * Sets caption
	 * @param c - new caption
	 */
	public void setCaption(String c){
		this.caption = c;
	}
	
	/**
	 * 
	 * @return Date - date picture was taken
	 */
	public Calendar getDate(){
		return this.dateTaken;
	}
	
	public void setDate(long input){
		dateTaken.setTimeInMillis(input);
		dateTaken.set(Calendar.MILLISECOND, 0);
	}
}

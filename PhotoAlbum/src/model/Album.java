package model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Album {
	private String albumName;
	public ArrayList<Photo> photos;
	public int numPhotos;
	public Calendar earliest;
	public Calendar latest;
	
	public Album(String albumName){
		this.albumName = albumName;
		this.numPhotos = 0;
		photos = new ArrayList<Photo>();
	}
	
	/**
	 * Getter for albumName
	 * @return this Album's name
	 */
	public String getName(){
		return this.albumName;
	}
	
	public void setName(String name){
		this.albumName = name;
	}
	
	/**
	 * Adds a photo to this album
	 * @param p - specified photo
	 */
	public void addPhoto(Photo p){
		photos.add(p);
		if(this.numPhotos == 0){
			this.earliest = p.getDate();
			this.latest = p.getDate();
		}else{
			if(p.getDate().before(earliest))
				this.earliest = p.getDate();
			if(p.getDate().after(latest))
				this.latest = p.getDate();
		}
		this.numPhotos++;
	}
	
	public void deletePhoto(Photo p){
		photos.remove(p);
		this.numPhotos--;
	}
	
	public String toString(){
		return this.albumName+": "+this.numPhotos+" photos";
	}
}

/**
 * @author Manan, Mehul
 */
package view;
	
import java.io.File;
import java.io.IOException;

import controller.AdminController;
import controller.AlbumController;
import controller.LoginController;
import controller.UserController;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.User;
import javafx.scene.Scene;
import javafx.scene.control.TitledPane;


public class PhotoAlbum extends Application {
	
	public static Stage stage; // The stage, need this to change scenes
	public static Stage adminStage; // This will be used for the admin view
	public static Stage userStage; // This will be used for the user view
	public static Stage albumStage; // This will be used for the album view
	public Scene scene1; // Stores the scenes, need to be public to change
	public static Scene scene2, scene3, scene4;
	public static File homeFolder;
	public static File currentFolder;
	public User currentUser; // Tracks current user
	
	
	/**
	 * Start it up
	 */
	@Override
	public void start(Stage primaryStage) throws IOException {
		stage = primaryStage;
		adminStage = new Stage();
		userStage = new Stage();
		albumStage = new Stage();
		homeFolder = new File(System.getProperty("user.dir"));
		currentFolder = new File(System.getProperty("user.dir"));
		
		// Login screen
		FXMLLoader loader = new FXMLLoader(); // Load FXML
		loader.setLocation(getClass().getResource("/controller/Login.fxml"));
		TitledPane login = (TitledPane)loader.load();
		
		// Administrator view
		FXMLLoader loader2 = new FXMLLoader(); // Load FXML
		loader2.setLocation(getClass().getResource("/controller/Userlist.fxml"));
		TitledPane adminView = (TitledPane)loader2.load();
		final AdminController adController = (AdminController)loader2.getController();
	    adminStage.setOnShown(new EventHandler<WindowEvent>(){
	    	@Override
	        public void handle(WindowEvent window)
	        {
	    		//System.out.println("Handling");
	    		try{
					adController.loadUserList(); // PERSISTENCE
				}catch (Exception e){
					//System.out.println("ERROR.");
				}
	        }
	    });
		
		// User view
		FXMLLoader loader3 = new FXMLLoader(); // Load FXML
		loader3.setLocation(getClass().getResource("/controller/Albums.fxml"));
		TitledPane userView = (TitledPane)loader3.load();
		final UserController uController = (UserController)loader3.getController();
	    userStage.setOnShown(new EventHandler<WindowEvent>(){
	    	@Override
	        public void handle(WindowEvent window)
	        {
	    		//System.out.println("Handling");
	    		try{
					uController.loadAlbumList(); // PERSISTENCE
				}catch (Exception e){
					//System.out.println("ERROR.");
				}
	        }
	    });
		
		// Album view
		FXMLLoader loader4 = new FXMLLoader(); // Load FXML
		loader4.setLocation(getClass().getResource("/controller/Photos.fxml"));
		TitledPane albumView = (TitledPane)loader4.load();
		final AlbumController aController = (AlbumController)loader4.getController();
	    albumStage.setOnShown(new EventHandler<WindowEvent>(){
	    	@Override
	        public void handle(WindowEvent window)
	        {
	    		//System.out.println("Handling");
	    		try{
					aController.loadPhotos(); // PERSISTENCE
				}catch (Exception e){
					//System.out.println("ERROR.");
				}
	        }
	    });
		
		// Make controller to load users, albums, and photos for persistence!
		final LoginController controller = (LoginController)loader.getController();
		
		scene1 = new Scene(login);
		scene2 = new Scene(adminView);
		scene3 = new Scene(userView);
		scene4 = new Scene(albumView);
		
		primaryStage.setScene(scene1);
		primaryStage.setResizable(false);  
		primaryStage.show();
		
		adminStage.setScene(scene2);
		adminStage.setResizable(false);
		
		userStage.setScene(scene3);
		userStage.setResizable(false);
		
		albumStage.setScene(scene4);
		albumStage.setResizable(false);
	}
	
	
	
	public static void main(String[] args) {
		launch(args);
	}
}

/**
 * @author Manan, Mehul
 */
package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.User;
import view.PhotoAlbum;

public class UserController {
	@FXML ListView<Album> list;
	@FXML TextField name; // Album name
	@FXML TextField search1; // Search field 1
	@FXML TextField search2; // Search field 2
	
	
	// Buttons
	@FXML Button create;
	@FXML Button delete;
	@FXML Button rename;
	@FXML Button open;
	
	User currentUser; // Stores the current user
	int listIndex = 0; // Track where in the list we are
	
	
	/**
	 * Need to exit back to login screen
	 */
	public void logOut(){
		System.setProperty("user.dir", PhotoAlbum.homeFolder.getAbsolutePath());
		PhotoAlbum.currentFolder = PhotoAlbum.homeFolder;
		listIndex = 0; // Reset index
		list.getItems().clear(); // Empty the list
		PhotoAlbum.stage.show(); // Back to login
		name.setText("");
		currentUser = null; // Resets, garbage collector will clear
		PhotoAlbum.userStage.close();
	}
	
	/**
	 * 
	 * @param e - something from the list was chosen
	 */
	public void listChoice(MouseEvent e) {
		listIndex = list.getSelectionModel().getSelectedIndex();
		if(listIndex < 0){ // empty list
			return;
		}
		// Show the chosen album's details
		name.setText(list.getItems().get(listIndex).getName());

	}
	
	/**
	 * Every album will be written into a text-file, so we just have to load every textfile.
	 */
	public void loadAlbumList(){
		String[] currentDir;
		
		currentDir = PhotoAlbum.currentFolder.list();
		String path = PhotoAlbum.currentFolder.getPath();
		
		currentUser = new User(path.substring(path.lastIndexOf('\\') + 1));
		//System.out.println("currentUser = " + currentUser); // FLAG
		//System.out.println("Loading albums...");
		
		list.getItems().clear();
		for(int x=0; x<currentDir.length; x++){
			if(currentDir[x].contains(".txt")){
				Album tempA = new Album(currentDir[x].substring(0, currentDir[x].indexOf('.')));
				fillAlbum(tempA);
				currentUser.addAlbum(tempA);
				list.getItems().add(tempA);
			}
		}
	}
	
	/**
	 * This method takes the given album and loads all of the (Photo) objects it contains from
	 *  the corresponding textfile
	 * @param tempA: the album to fill
	 */
	private void fillAlbum(Album tempA) {
		String filename;
		File albumFile;
		Photo tempPhoto;
		ObjectInputStream inflow;
		
		filename = new String(PhotoAlbum.currentFolder.getPath());
		filename = filename.concat("\\");
		filename = filename.concat(tempA.getName());
		filename = filename.concat(".txt");
		
		albumFile = new File(filename);// the file will be named <albumname>.txt
		System.out.println("Loading album: "+albumFile.getAbsolutePath());
		try {
			inflow = new ObjectInputStream(new FileInputStream(albumFile));
		} catch (IOException e1) {
			//e1.printStackTrace();
			//System.out.println("Could not create inputstream");
			filename = null;
			albumFile = null;
			inflow = null;
			return;
		}

		try {//Now we go through and load all the photos stored in the file
			int x = (int)inflow.readObject();
			while((tempPhoto = (Photo)inflow.readObject()) != null){
				tempA.addPhoto(tempPhoto);
			}
			//System.out.println("Exited loading loop");
		} catch (ClassNotFoundException | IOException e) {
			//System.out.println("Caught exception in fillAlbum()");
			//e.printStackTrace();
		}
		
		try {
			//System.out.println("Finished loading album");
			inflow.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Set the textfield to show what is currently selected
	 */
	public void setTextToCurrent(){
		if(list.getItems().size() == 0){
			return;
		}
		name.setText(list.getItems().get(listIndex).getName());
	}	
	
	/**
	 * Creates an album with the given name in the textfield
	 * @param e - Button pressed
	 */	
	public void createAlbum(ActionEvent e){
		String filename;
		File albumFile;
		String[] currentDir;
		
		// Create pop-up window alert
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setHeaderText("Error");
		alert.setResizable(true);
		
		currentDir = PhotoAlbum.currentFolder.list();
		// catch duplicates
		for(int i = 0; i < list.getItems().size(); i++){
			if(list.getItems().get(i).getName().equals(name.getText())){
				alert.setContentText("This user already exists.");
				alert.showAndWait();
				setTextToCurrent();
				return;
			}
		}
		
		

		System.out.println("Creating album: "+name.getText());
		filename = new String(PhotoAlbum.currentFolder.getPath());
		filename = filename.concat("\\");
		filename = filename.concat(name.getText());
		filename = filename.concat(".txt");
		
		albumFile = new File(filename);// the file will be named <albumname>.txt
		try {
			//System.out.println(filename);
			albumFile.createNewFile();
			albumFile.setWritable(true);
			list.getItems().add(new Album(name.getText()));
		} catch (IOException e1) {
			System.out.println("Error creating album");
			e1.printStackTrace();
		}
		

		listIndex = list.getItems().size() - 1; // New index
		list.getSelectionModel().clearAndSelect(listIndex);
		
		//This loop sorts the list alphabetically
		while(listIndex > 0){
			if(list.getItems().get(listIndex).getName().compareToIgnoreCase(list.getItems().get(listIndex - 1).getName()) < 0){
				// Need to move up list, three way swap
				Album temp = list.getItems().get(listIndex);
				list.getItems().set(listIndex, list.getItems().get(listIndex-1));
				list.getItems().set(listIndex-1, temp);
				listIndex--;
				list.getSelectionModel().clearAndSelect(listIndex);
			}
			else{
				break; // In right spot
			}
		}
		
		
		//Here I insert a null value into the file because the ObjectInputStream won't close properly otherwise
		try {
			ObjectOutputStream outflow = new ObjectOutputStream(new FileOutputStream(albumFile));
			outflow.writeObject((int)0);
			outflow.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		loadAlbumList();

	}
	
	
	/**
	 * Deletes the selected album
	 * @param e - Button pressed
	 */	
	public void deleteAlbum(ActionEvent e){
		String filename;
		File albumFile;
		
		filename = new String(PhotoAlbum.currentFolder.getPath());
		filename = filename.concat("\\");
		filename = filename.concat(list.getItems().get(listIndex).getName());
		filename = filename.concat(".txt");
		//System.out.println("Deleting: "+filename);

		try{
			list.getItems().remove(listIndex);
			albumFile = new File(filename);
			if(albumFile.getAbsoluteFile().delete());
					//System.out.println("Album Deleted");
		} catch(IndexOutOfBoundsException i){
			//System.out.println(listIndex); // FLAG
		}
		
		if(listIndex > 0 && listIndex >= list.getItems().size()) listIndex--;
		
		if(list.getItems().size() != 0){
			//Choose next available thing on the list
			list.getSelectionModel().clearAndSelect(listIndex);
			setTextToCurrent();
		}
		else{
			name.setText("");
		}
		loadAlbumList();
	}
	
	
	/**
	 * Renames the selected album with the name given in the text field
	 * @param e - Button pressed
	 */	
	public void renameAlbum(ActionEvent e){
		String filename;
		File albumFile;
		
		// Create pop-up window alert
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setHeaderText("Error");
		alert.setResizable(true);
		
		// catch duplicates
		for(int i = 0; i < list.getItems().size(); i++){
			if(list.getItems().get(i).getName().equals(name.getText())){
				alert.setContentText("This user already exists.");
				alert.showAndWait();
				setTextToCurrent();
				return;
			}
		}
		
		filename = new String(PhotoAlbum.currentFolder.getPath());
		filename = filename.concat("\\");
		filename = filename.concat(list.getItems().get(listIndex).getName());
		filename = filename.concat(".txt");

		albumFile = new File(filename);

		filename = new String(PhotoAlbum.currentFolder.getPath());
		filename = filename.concat("\\");
		filename = filename.concat(name.getText());
		filename = filename.concat(".txt");
//		System.out.println("Renaming: "+list.getItems().get(listIndex).getName()+" to "+name.getText());
		
		albumFile.renameTo(new File(filename));

		Album temp = list.getItems().get(listIndex);
		list.getItems().remove(listIndex);
		temp.setName(name.getText());
		list.getItems().add(temp);
		
		listIndex = list.getItems().size() - 1; // New index
		list.getSelectionModel().clearAndSelect(listIndex);
		
		//This loop sorts the list alphabetically
		while(listIndex > 0){
			if(list.getItems().get(listIndex).getName().compareToIgnoreCase(list.getItems().get(listIndex - 1).getName()) < 0){
				// Need to move up list, three way swap
				Album temp2 = list.getItems().get(listIndex);
				list.getItems().set(listIndex, list.getItems().get(listIndex-1));
				list.getItems().set(listIndex-1, temp2);
				listIndex--;
				list.getSelectionModel().clearAndSelect(listIndex);
			}
			else{
				break; // In right spot
			}
		}
	}
	
	/**
	 * Opens an Album to see pictures
	 * @param e - Button pressed
	 */
	public void openAlbum(ActionEvent e){
		if(list.getSelectionModel().getSelectedIndex() >= 0 && list.getSelectionModel().getSelectedIndex() < list.getItems().size()){
			AlbumController.currentAlbum = name.getText();
		}
		else{
			return;
		}
		listIndex = 0; // Reset index
		list.getItems().clear(); // Empty the list
		name.setText("");
		PhotoAlbum.albumStage.show();
		PhotoAlbum.userStage.hide();
	}
	
	/**
	 * Search by date
	 * @throws IOException 
	 */
	public void searchDates() throws IOException{
		boolean validDate = true; // Make sure the inputs are valid
		
		if(search1.getText().length() != 10 || search1.getText().indexOf('/') != 2 || search1.getText().indexOf('/', 3) != 5){
			//System.out.println("Search1 is invalid."); // FLAG
			search1.setText("");
			search1.setPromptText("(MM/DD/YYYY)");
			validDate = false;
		}
		if(search2.getText().length() != 10 || search2.getText().indexOf('/') != 2 || search2.getText().indexOf('/', 3) != 5){
			//System.out.println("Search2 is invalid."); // FLAG
			search2.setText("");
			search2.setPromptText("(MM/DD/YYYY)");
			validDate = false;
		}
		if(!validDate) return; // Check 1
		else{
			//System.out.println("Inputs are valid dates!"); // FLAG
		}
		// Date one valid format?
		if(Integer.parseInt(search1.getText().substring(6)) < 0){
			search1.setText("");
			search1.setPromptText("(MM/DD/YYYY)");
			validDate = false;
		}
		if(Integer.parseInt(search1.getText().substring(0,2)) < 0 || Integer.parseInt(search1.getText().substring(0,2)) > 11){
			search1.setText("");
			search1.setPromptText("(MM/DD/YYYY)");
			validDate = false;
		}
		if(Integer.parseInt(search1.getText().substring(3,5)) < 0 || Integer.parseInt(search1.getText().substring(3,5)) > 30){
			search1.setText("");
			search1.setPromptText("(MM/DD/YYYY)");
			validDate = false;
		}
		// Date two valid format?
		if(Integer.parseInt(search2.getText().substring(6)) < 0){
			search2.setText("");
			search2.setPromptText("(MM/DD/YYYY)");
			validDate = false;
		}
		if(Integer.parseInt(search2.getText().substring(0,2)) < 0 || Integer.parseInt(search2.getText().substring(0,2)) > 11){
			search2.setText("");
			search2.setPromptText("(MM/DD/YYYY)");
			validDate = false;
		}
		if(Integer.parseInt(search2.getText().substring(3,5)) < 0 || Integer.parseInt(search2.getText().substring(3,5)) > 30){
			search2.setText("");
			search2.setPromptText("(MM/DD/YYYY)");
			validDate = false;
		}
		
		if(!validDate) return; // Check 2
		
		
		ArrayList<Photo> results = new ArrayList<Photo>();
		
		Calendar date1 = Calendar.getInstance();
		date1.set(Calendar.MILLISECOND, 0);
		date1.set(Integer.parseInt(search1.getText().substring(6)), Integer.parseInt(search1.getText().substring(0,2)), Integer.parseInt(search1.getText().substring(3,5)));
		//System.out.println("First date: " + date1); // FLAG
		
		Calendar date2 = Calendar.getInstance();
		date2.set(Calendar.MILLISECOND, 0);
		date2.set(Integer.parseInt(search2.getText().substring(6)), Integer.parseInt(search2.getText().substring(0,2)), Integer.parseInt(search2.getText().substring(3,5)));
		//System.out.println("Second date: " + date2); // FLAG
		
		if(date1.compareTo(date2) > 0){
			//System.out.println("The first date should be earlier."); // FLAG
			search1.setText("");
			search1.setPromptText("(MM/DD/YYYY)");
			search2.setText("");
			search2.setPromptText("(MM/DD/YYYY)");
			return;
		}
		
		for(int i = 0; i < currentUser.albums.size(); i++){
			for(int j = 0; j < currentUser.albums.get(i).photos.size(); j++){
				//System.out.println(currentUser.albums.get(i).photos.get(j).getDate()); // FLAG
				//System.out.println(currentUser.albums.get(i).photos.get(j).getDate().compareTo(date1)); // FLAG
				if(currentUser.albums.get(i).photos.get(j).getDate().compareTo(date1) >= 0){
					//System.out.println(currentUser.albums.get(i).photos.get(j).getDate().compareTo(date2)); // FLAG
					if(currentUser.albums.get(i).photos.get(j).getDate().compareTo(date2) <= 0){
						results.add(currentUser.albums.get(i).photos.get(j)); // This photo is between specified dates
					}
				}
			}
		}
		
		System.out.println(results); // FLAG Prints out the arraylist of results
		
		// Search view
		Stage stage = new Stage();
		Scene scene;
		FXMLLoader loader = new FXMLLoader(); // Load FXML
		loader.setLocation(getClass().getResource("/controller/SearchResults.fxml"));
		TitledPane searchView = (TitledPane)loader.load();
		SearchController s = (SearchController)loader.getController();
		
		scene = new Scene(searchView);
		stage.setScene(scene);
		stage.setResizable(false);

		s.loadList(results);
		stage.showAndWait();
		
		loadAlbumList();
		
		/*
		// To save or not save
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Save or not");
		alert.setHeaderText("Would you like to save the results as their own album?");
		alert.setContentText("Yes/No");

		ButtonType yesButton = new ButtonType("Yes");
		ButtonType noButton = new ButtonType("No");

		alert.getButtonTypes().setAll(yesButton, noButton);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == yesButton){
		    // ... user chose "Yes" -> SAVE AS ALBUM, then show search results
			TextInputDialog dialog = new TextInputDialog("");
			dialog.setTitle("Album naming");
			dialog.setHeaderText("What do you want to name the album?");
			dialog.setContentText("Please enter the album name:");
			
			Optional<String> result2 = dialog.showAndWait();
			if (result2.isPresent()){
			    // Create an album with the name result2.get()
			}
			
		} else if (result.get() == noButton) {
		    // ... user chose "No" -> Show them search results
			
			// Search view
			Stage stage = new Stage();
			Scene scene;
			FXMLLoader loader = new FXMLLoader(); // Load FXML
			loader.setLocation(getClass().getResource("/controller/SearchResults.fxml"));
			TitledPane searchView = (TitledPane)loader.load();
			SearchController s = (SearchController)loader.getController();
			
			scene = new Scene(searchView);
			stage.setScene(scene);
			stage.setResizable(false);
			
			stage.show();
			s.loadList(results);
		} else {
		    return; // They exited the search
		}*/
	}
	
	/**
	 * Search by tags
	 * @throws IOException 
	 */
	public void searchTags() throws IOException{
		String tag1 = search1.getText();
		String tag2 = search2.getText();
		
		if(tag1 == null || tag2 == null){
			search1.setText("");
			search1.setPromptText("(MM/DD/YY)");
			search2.setText("");
			search2.setPromptText("(MM/DD/YY)");
			return;
		}
		
		ArrayList<Photo> results = new ArrayList<Photo>();
		
		// Search for the tags, if at least one is there, add the picture to the list
		for(int i = 0; i < currentUser.albums.size(); i++){
			for(int j = 0; j < currentUser.albums.get(i).photos.size(); j++){
				if(currentUser.albums.get(i).photos.get(j).hasTag(tag1) || currentUser.albums.get(i).photos.get(j).hasTag(tag2)){
					results.add(currentUser.albums.get(i).photos.get(j));
				}
			}
		}
		
		System.out.println(results); // FLAG Prints out the arraylist of results
		
		// Search view
		Stage stage = new Stage();
		Scene scene;
		FXMLLoader loader = new FXMLLoader(); // Load FXML
		loader.setLocation(getClass().getResource("/controller/SearchResults.fxml"));
		TitledPane searchView = (TitledPane)loader.load();
		SearchController s = (SearchController)loader.getController();
		
		scene = new Scene(searchView);
		stage.setScene(scene);
		stage.setResizable(false);

		s.loadList(results);
		stage.showAndWait();
		
		loadAlbumList();
	}
}

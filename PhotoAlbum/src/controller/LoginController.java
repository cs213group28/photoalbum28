/**
 * @author Manan, Mehul
 */
package controller;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import model.User;
import view.PhotoAlbum;

public class LoginController {
	@FXML TextField username;
	@FXML Button login;

	/**
	 * Log in
	 * @param e - button pressed
	 * @throws IOException
	 */
	public void logIn(ActionEvent e) throws IOException {
		Button b = (Button)e.getSource();
		if(b != login) return;
		
		boolean isValidUsername = false; // Check if this user exists
		
		// Admin account, instructions specify all lower case
		if(username.getText().equals("admin")){
			//System.out.println("Logging in admin..."); // FLAG
			PhotoAlbum.adminStage.show();
			username.setText(""); // Clear text
			PhotoAlbum.stage.close();
			return;
		}
		
		isValidUsername = checkUser();
		
		
		if(isValidUsername){
			//System.out.println("Logging in user: " + username.getText()); // FLAG
			//System.out.println("Previously working in: "+System.getProperty("user.dir"));
			String newDir = new String();
			newDir = PhotoAlbum.currentFolder.getPath();
			newDir = newDir.concat("\\");
			newDir = newDir.concat(username.getText());
			PhotoAlbum.currentFolder = new File(newDir);
			System.setProperty("user.dir", newDir);//Change the working directory
			//System.out.println("Now working in folder: "+System.getProperty("user.dir"));
			PhotoAlbum.userStage.show();
			username.setText(""); // Clear text
			PhotoAlbum.stage.close();
		}
		else{
			//System.out.println("No user exists with username '" + username.getText() + "'");
		}
	}

	/**
	 * This method will search for the given username in userlist.txt.
	 * @return If the user exists, meaning the username was found in userlist.txt, it returns true.
	 */
	private boolean checkUser() {
		ObjectInputStream inflow;
		try {
			inflow = new ObjectInputStream(new FileInputStream("userlist.txt"));
		} catch (IOException e) {
			System.err.println("ERROR: could not load list of users");
			e.printStackTrace();
			return false;
		}
		
		String tempstr;
		try {
			while((tempstr = (String)inflow.readObject())!= null){
				if(username.getText().equals(tempstr))
					return true;
			}
		} catch (Exception e) {
			//System.err.println("ERROR: problem reading list of users");
			//e.printStackTrace();
		}
		
		return false;
	}
	
}


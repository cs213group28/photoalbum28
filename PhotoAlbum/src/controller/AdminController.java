/**
 * @author Manan, Mehul
 */
package controller;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import model.User;
import view.PhotoAlbum;

public class AdminController {
	@FXML ListView<User> list;
	@FXML TextField username;
	@FXML Button add;
	@FXML Button delete;
	
	int listIndex = 0; // Track where in the list we are
	
	/**
	 * Need to exit back to login screen
	 */
	public void logOut(){
		listIndex = 0; // Reset index
		list.getItems().clear(); // Empty the list
		PhotoAlbum.stage.show(); // Back to login
		username.setText("");
		PhotoAlbum.stage.show();
		PhotoAlbum.adminStage.close();
	}
	
	
	/**
	 * 
	 * @param e - something from the list was chosen
	 */
	public void listChoice(MouseEvent e) {
		listIndex = list.getSelectionModel().getSelectedIndex();
		if(listIndex < 0){ // empty list
			return;
		}
		// Show the chosen song's details
		username.setText(list.getItems().get(listIndex).getName());

	}
	
	public void setTextToCurrent(){
		if(list.getItems().size() == 0){
			return;
		}
		username.setText(list.getItems().get(listIndex).getName());
	}
	
	/**
	 * Add a User
	 * @param e - add button pressed
	 */	
	public void addUser(ActionEvent e){
		Button b = (Button)e.getSource();
		
		// Create pop-up window alert
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setHeaderText("Error");
		alert.setResizable(true);
		
		if(!b.equals(add)){
			System.err.println("Wrong button."); // FLAG
		}
		
		if(username.getText().length() <= 0){
			alert.setContentText("Please input a valid username.");
			alert.showAndWait();
			setTextToCurrent();
			return;
		}

		
		// catch duplicates
		for(int i = 0; i < list.getItems().size(); i++){
			if(list.getItems().get(i).getName().equals(username.getText())){
				alert.setContentText("This user already exists.");
				alert.showAndWait();
				setTextToCurrent();
				return;
			}
		}
		
		// Add if made it this far
		list.getItems().add(new User(username.getText()));
		(new File(username.getText())).mkdir();

		
		listIndex = list.getItems().size() - 1; // New index
		list.getSelectionModel().clearAndSelect(listIndex);
		
		//This loop sorts the list alphabetically
		while(listIndex > 0){
			if(list.getItems().get(listIndex).getName().compareToIgnoreCase(list.getItems().get(listIndex - 1).getName()) < 0){
				// Need to move up list, three way swap
				User temp = list.getItems().get(listIndex);
				list.getItems().set(listIndex, list.getItems().get(listIndex-1));
				list.getItems().set(listIndex-1, temp);
				listIndex--;
				list.getSelectionModel().clearAndSelect(listIndex);
			}
			else{
				break; // In right spot
			}
		}
		
		//Write the list to a text file and re-load the ListView
		try {
			//System.out.println("Running write and load");
			writeUserList();
			loadUserList();
		} catch (IOException e1) {
			System.err.println("Could not write out the user list.");
			e1.printStackTrace();
		}
	}
	
	/**
	 * This method will load the list of users from the text file "userlist.txt".
	 * It will load them into the ListView of users.
	 * @throws IOException
	 */
	public void loadUserList() throws IOException {
		ObjectInputStream inflow;
		//System.out.println("Loading");
		
		inflow = new ObjectInputStream(new FileInputStream("userlist.txt"));
		
		if (list == null){//System.out.println("The list was empty");
		}
		else if(list.getItems().size()>0){
			//System.out.println("Clearing the list");
				/*list.getSelectionModel().selectAll();
				list.getSelectionModel().clearSelection();*/
				list.getItems().clear();
		}//We've cleared the list, now to re-fill
		String tempstr;
		try {
			while((tempstr = (String)inflow.readObject())!= null){
				list.getItems().add(new User(tempstr));
				//System.out.println("Added a user to the list");
			}
		} catch (ClassNotFoundException e) {
			//System.out.println("Reading error");
			e.printStackTrace();
		} catch (EOFException e){
			//System.out.println("End of File");
		}
		
		inflow.close();
	}
	
	/**
	 * This method will take the current list of users from the ListView and write it to a text file.
	 * The old version of the text file will be deleted because it is obsolete, and the new version
	 * 		will reflect whatever changes the user made.
	 * @throws IOException
	 */
	private void writeUserList() throws IOException {
		// TODO Auto-generated method stub
		ObjectOutputStream out;
		File tempf;
		tempf = new File("userlist.txt");
		if(tempf.exists()){
			tempf.delete();
		}
		
		try {
			out = new ObjectOutputStream(new FileOutputStream("userlist.txt"));
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		
		for(int z=0; z<list.getItems().size(); z++){
			out.writeObject(list.getItems().get(z).getName());
		}
		
		out.close();

	}

	/**
	 * Delete a User
	 * @param e - delete button pressed
	 */
	public void deleteUser(ActionEvent e){
		String[] currentDir;
		Button b = (Button)e.getSource();
		
		// Create pop-up window alert
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setHeaderText("Error");
		alert.setResizable(true);
		
		if(!b.equals(delete)){
			//System.out.println("Wrong button."); // FLAG
		}
		
		try{//We have to delete all files within the folder before we can delete the folder
			String newDir = new String();
			newDir = PhotoAlbum.currentFolder.getPath();
			newDir = newDir.concat("\\");
			newDir = newDir.concat(username.getText());
			PhotoAlbum.currentFolder = new File(newDir);
			System.setProperty("user.dir", newDir);//Change the working directory so we can delete
			
			currentDir = PhotoAlbum.currentFolder.list();
			if(currentDir != null){
				for(int x = 0; x < currentDir.length; x++){
					newDir = PhotoAlbum.currentFolder.getPath();
					newDir = newDir.concat("\\");
					newDir = newDir.concat(currentDir[x]);
					//System.out.println("Deleting: "+"  : "+newDir+(new File(newDir)).delete());
				}
			}

			System.setProperty("user.dir", PhotoAlbum.homeFolder.getAbsolutePath());
			PhotoAlbum.currentFolder = PhotoAlbum.homeFolder;
			if(!(new File(list.getItems().get(listIndex).getName())).delete()){
				//System.out.println("Could not delete user");
				return;
			}
			list.getItems().remove(listIndex);
		} catch(IndexOutOfBoundsException i){
			//System.out.println(listIndex); // FLAG
		}
		
		if(listIndex > 0 && listIndex >= list.getItems().size()) listIndex--;
		
		if(list.getItems().size() != 0){
			//Choose next available thing on the list
			list.getSelectionModel().clearAndSelect(listIndex);
			setTextToCurrent();
		}
		else{
			username.setText("");
		}

		//Write the list to a text file and re-load the ListView
		try {
			//System.out.println("Running write and load");
			writeUserList();
			loadUserList();
		} catch (IOException e1) {
			System.err.println("Could not write out the user list.");
			e1.printStackTrace();
		}
		
	}
	
}

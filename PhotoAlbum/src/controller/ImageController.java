/**
 * @author Manan, Mehul
 */
package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.Album;
import model.Photo;
import view.PhotoAlbum;

public class ImageController {
	@FXML ImageView image;
	@FXML TextField caption; // Address of photo
	@FXML TextField tags; // Used for captions/tags
	@FXML TextField date; // Album name
	
	// Buttons
	@FXML Button previous;
	@FXML Button next;

	
	Album tempA; // What album we are in
	int currentIndex;
	
	
	/**
	 * Go to previous photo in album
	 * @param e - previous button pressed
	 */
	public void previous(ActionEvent e){
		currentIndex--;
		if(currentIndex < 0)
			currentIndex = tempA.photos.size()-1;
		//System.out.println(currentIndex);
		setState();
	}
	
	private void setState() {
		Image temp;
		ArrayList<String> tagSet;
		String tagWord;
		
		tagSet = tempA.photos.get(currentIndex).getTags();
		tagWord = new String("");
		try {
			temp = new Image(new FileInputStream(tempA.photos.get(currentIndex).getAddress()));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}
		image.setImage(temp);
		caption.setText(tempA.photos.get(currentIndex).getCaption());
		for(int x=0; x<tagSet.size(); x++){
			tagWord = tagWord.concat(tagSet.get(x));
			tagWord = tagWord.concat(", ");
		}
		tags.setText(tagWord);
		date.setText(tempA.photos.get(currentIndex).getDate().getTime().toString());
	}

	/**
	 * Go to next photo in album
	 * @param e - next button pressed
	 */
	public void next(ActionEvent e){
		currentIndex++;
		if(currentIndex == tempA.photos.size())
			currentIndex = 0;
		//System.out.println(currentIndex);
		setState();
	}

	/**
	 * Load photos to be viewed as HBoxes 
	 */
	public void loadPhotos() {
		String filename;
		File albumFile;
		Photo tempPhoto;
		ObjectInputStream inflow;
		//System.out.println("Went fullscreen from album: "+AlbumController.currentAlbum);
		tempA = new Album(AlbumController.currentAlbum);
		currentIndex = AlbumController.index;

		filename = new String(PhotoAlbum.currentFolder.getPath());
		filename = filename.concat("\\");
		filename = filename.concat(AlbumController.currentAlbum);
		filename = filename.concat(".txt");
		//System.out.println(filename);
		
		albumFile = new File(filename);

		try {
			inflow = new ObjectInputStream(new FileInputStream(albumFile));
		} catch (IOException e1) {
			//e1.printStackTrace();
			//System.out.println("Could not create inputstream");
			filename = null;
			albumFile = null;
			inflow = null;
			return;
		}
		
		try {
			int x = (int)inflow.readObject();
			while((tempPhoto = (Photo)inflow.readObject()) != null){
				tempA.addPhoto(tempPhoto);
			}
			//System.out.println("Flag!");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		//System.out.println(tempA.photos.get(currentIndex).getAddress());
		setState();
		
		try {
			//System.out.println("Finished re-loading album for fullscreen");
			inflow.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

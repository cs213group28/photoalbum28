/**
 * @author Manan, Mehul
 */
package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.Album;
import model.Photo;
import model.User;
import view.PhotoAlbum;

public class AlbumController {
	@FXML ListView<HBox> list;
	@FXML TextField photoLocation; // Address of photo
	@FXML TextField captionOrTag; // Used for captions/tags
	@FXML TextField destinationAlbum; // Album name
	
	// Buttons
	@FXML Button addPhoto;
	@FXML Button removePhoto;
	@FXML Button movePhoto;
	@FXML Button addTag;
	@FXML Button deleteTag;
	@FXML Button caption;
	@FXML Button display;
	@FXML Button back;
	
	
	int listIndex = 0; // Track where in the list we are
	User currentUser;
	Album tempA;
	static String currentAlbum;
	static int index;
	public Stage fullscreen;
	/**
	 * 
	 * @param e - something from the list was chosen
	 */
	public void listChoice(MouseEvent e) {
		listIndex = list.getSelectionModel().getSelectedIndex();
		index = listIndex;
		if(listIndex < 0){ // empty list
			return;
		}
		// Show the chosen photo's details
		captionOrTag.setText(tempA.photos.get(list.getSelectionModel().getSelectedIndex()).getCaption());
	}
	
	/**
	 * Set the textfield to show what is currently selected
	 */
	public void setTextToCurrent(){
		if(list.getItems().size() == 0){
			return;
		}
		Text temp = (Text) list.getItems().get(listIndex).getChildren().get(1);
		photoLocation.setText(temp.getText());
	}	
	
	/**
	 * Adds photo at the specified address to album
	 * @param e - add pressed
	 */
	public void addPhoto(ActionEvent e){
		Image pic;
		Photo addition;
		String filename = photoLocation.getText();
		//System.out.println("Loading photo at: "+filename);
		
		// Create pop-up window alert
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Alert");
		alert.setHeaderText("Error");
		alert.setResizable(true);
		
		//Check if the image at that address has already been loaded into the album
		for(int x=0; x<tempA.photos.size(); x++){
			if(tempA.photos.get(x).getAddress().equals(filename)){
				alert.setContentText("That photo is already in the album!");
				alert.showAndWait();
				setTextToCurrent();
				return;
			}
		}
		
		try {//We load it into an Image just to see if the file actually exists
			pic = new Image(new FileInputStream(filename));
		} catch (FileNotFoundException e1) {
			alert.setContentText("File not found!");
			alert.showAndWait();
			setTextToCurrent();
			return;
			//e1.printStackTrace();
		} catch (Exception e1){
			alert.setContentText("Could not load file!");
			alert.showAndWait();
			setTextToCurrent();
			return;
		}
		
		addition = new Photo(filename.substring(filename.lastIndexOf('\\')+1, filename.lastIndexOf('.')), filename);
		long timeVal = (new File(filename).lastModified());
		addition.setDate(timeVal);
		//System.out.println(addition.getDate().getTime());
		tempA.addPhoto(addition);
		
		writePhotos();
		loadPhotos();
	}
	
	private void writePhotos() {
		String filename;
		ObjectOutputStream out;
		File albumFile;

		filename = new String(PhotoAlbum.currentFolder.getPath());
		filename = filename.concat("\\");
		filename = filename.concat(currentAlbum);
		filename = filename.concat(".txt");
		
		albumFile = new File(filename);
		if(albumFile.exists()){
			albumFile.delete();
		}
		try {
			out = new ObjectOutputStream(new FileOutputStream(filename));
			out.writeObject((int)0);
			for(int x=0; x<tempA.photos.size(); x++){
				out.writeObject((Photo)tempA.photos.get(x));
			}
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return;
	}

	/**
	 * Deletes selected photo
	 * @param e - remove pressed
	 */
	public void removePhoto(ActionEvent e){
		//Text temp = (Text) list.getItems().get(listIndex).getChildren().get(1);
		//System.out.println(tempA.photos.get(listIndex).getName());
		
		tempA.photos.remove(listIndex);
		writePhotos();
		loadPhotos();
	}
	
	/**
	 * Moves photo from the current album to specified album IF it is in current album
	 * @param e - move pressed
	 */
	public void movePhoto(ActionEvent e){
		Album dest;
		
		dest = new Album(destinationAlbum.getText());
		fillAlbum(dest);
		
		for(int x=0; x<dest.photos.size(); x++){
			if(tempA.photos.get(listIndex).getAddress().equals(dest.photos.get(x).getAddress())){
				//System.out.println("That image is already in the album");
				return;
			}
		}
		
		dest.addPhoto(tempA.photos.get(listIndex));
		tempA.photos.remove(listIndex);
		writePhotos();
		loadPhotos();
		
		String filename;
		ObjectOutputStream out;
		File albumFile;

		filename = new String(PhotoAlbum.currentFolder.getPath());
		filename = filename.concat("\\");
		filename = filename.concat(destinationAlbum.getText());
		filename = filename.concat(".txt");
		
		albumFile = new File(filename);
		if(albumFile.exists()){
			albumFile.delete();
		}
		try {
			out = new ObjectOutputStream(new FileOutputStream(filename));
			out.writeObject((int)0);
			for(int x=0; x<dest.photos.size(); x++){
				out.writeObject((Photo)dest.photos.get(x));
			}
			out.close();
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		return;
	}
	
	/**
	 * This method takes the given album and loads all of the (Photo) objects it contains from
	 *  the corresponding textfile
	 * @param tempA: the album to fill
	 */
	private void fillAlbum(Album tempA) {
		String filename;
		File albumFile;
		Photo tempPhoto;
		ObjectInputStream inflow;
		
		filename = new String(PhotoAlbum.currentFolder.getPath());
		filename = filename.concat("\\");
		filename = filename.concat(tempA.getName());
		filename = filename.concat(".txt");
		
		albumFile = new File(filename);// the file will be named <albumname>.txt
		//System.out.println("Loading album: "+albumFile.getAbsolutePath());
		try {
			inflow = new ObjectInputStream(new FileInputStream(albumFile));
		} catch (IOException e1) {
			//e1.printStackTrace();
			//System.out.println("Could not create inputstream");
			filename = null;
			albumFile = null;
			inflow = null;
			return;
		}

		try {//Now we go through and load all the photos stored in the file
			int x = (int)inflow.readObject();
			while((tempPhoto = (Photo)inflow.readObject()) != null){
				tempA.addPhoto(tempPhoto);
			}
			//System.out.println("Exited loading loop");
		} catch (ClassNotFoundException | IOException e) {
			//System.out.println("Caught exception in fillAlbum()");
			//e.printStackTrace();
		}
		
		try {
			//System.out.println("Finished loading album");
			inflow.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Adds tag to the selected photo
	 * @param e - add tag pressed
	 */
	public void addTag(ActionEvent e){
		if(list.getSelectionModel().getSelectedIndex() < 0 || list.getSelectionModel().getSelectedIndex() > list.getItems().size()) return;
		if(captionOrTag.getText() == null || captionOrTag.getText().length() == 0) return;
		
		tempA.photos.get(list.getSelectionModel().getSelectedIndex()).addTag(captionOrTag.getText());
	
		// Save
		writePhotos();
		loadPhotos();
	}
	
	/**
	 * Deletes tag from the selected photo
	 * @param e - delete tag pressed
	 */
	public void deleteTag(ActionEvent e){
		if(list.getSelectionModel().getSelectedIndex() < 0 || list.getSelectionModel().getSelectedIndex() > list.getItems().size()) return;
		if(captionOrTag.getText() == null || captionOrTag.getText().length() == 0) return;
		
		tempA.photos.get(list.getSelectionModel().getSelectedIndex()).deleteTag(captionOrTag.getText());		
	
		// Save
		writePhotos();
		loadPhotos();
	}
	
	/**
	 * Adds caption to the selected photo
	 * @param e - caption pressed
	 */
	public void caption(ActionEvent e){
		if(list.getSelectionModel().getSelectedIndex() < 0 || list.getSelectionModel().getSelectedIndex() > list.getItems().size()) return;
		if(captionOrTag.getText() == null || captionOrTag.getText().length() == 0) return;
		
		tempA.photos.get(list.getSelectionModel().getSelectedIndex()).setCaption(captionOrTag.getText());
	
		// Save
		writePhotos();
		loadPhotos();
	}
	
	/**
	 * Goes into full display mode
	 * @param e - display pressed
	 */
	public void displayPhoto(ActionEvent e){
		fullscreen = new Stage();
		TitledPane fullView;
		FXMLLoader loader = new FXMLLoader(); // Load FXML
		loader.setLocation(getClass().getResource("/controller/ImageDisplay.fxml"));
		try {
			fullView = (TitledPane)loader.load();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}
		final ImageController iController = (ImageController)loader.getController();
	    fullscreen.setOnShown(new EventHandler<WindowEvent>(){
	    	@Override
	        public void handle(WindowEvent window)
	        {
	    		//System.out.println("Handling");
	    		try{
					iController.loadPhotos(); // PERSISTENCE
				}catch (Exception e){
					//System.out.println("ERROR.");
					e.printStackTrace();
				}
	        }
	    });
		Scene fullscene = new Scene(fullView);
		fullscreen.setScene(fullscene);
		fullscreen.setResizable(true);
		fullscreen.setAlwaysOnTop(true);
		fullscreen.show();
	}
	
	/**
	 * Goes back to userView
	 * @param e - back pressed
	 */
	public void back(ActionEvent e){
		listIndex = 0; // Reset index
		if(list.getItems() != null) list.getItems().clear(); // Empty the list
		photoLocation.setText("");
		captionOrTag.setText("");
		destinationAlbum.setText("");
		PhotoAlbum.albumStage.close();
		PhotoAlbum.userStage.show();
	}
	
	/**
	 * Load photos
	 */
	public void loadPhotos() {
		String filename;
		File albumFile;
		Photo tempPhoto;
		ObjectInputStream inflow;
		ImageView thumbnail;
		HBox listItem;
		String path = PhotoAlbum.currentFolder.getPath();
		
		if(list!=null)
			list.getItems().clear();
		
		currentUser = new User(path.substring(path.lastIndexOf('\\') + 1));
		//System.out.println("\n Viewing album: "+currentAlbum);
		tempA = new Album(currentAlbum);
		
		filename = new String(PhotoAlbum.currentFolder.getPath());
		filename = filename.concat("\\");
		filename = filename.concat(currentAlbum);
		filename = filename.concat(".txt");
		
		albumFile = new File(filename);
		
		try {
			inflow = new ObjectInputStream(new FileInputStream(albumFile));
		} catch (IOException e1) {
			//e1.printStackTrace();
			//System.out.println("Could not create inputstream");
			filename = null;
			albumFile = null;
			inflow = null;
			return;
		}

		try {//Now we go through and load all the photos stored in the file
			int x = (int)inflow.readObject();
			while((tempPhoto = (Photo)inflow.readObject()) != null){
				tempA.addPhoto(tempPhoto);

				thumbnail = new ImageView(new Image(new FileInputStream(tempPhoto.getAddress())));
				thumbnail.setFitWidth(100);
				thumbnail.setPreserveRatio(true);
				
				listItem = new HBox();
				listItem.getChildren().add(thumbnail);
				listItem.getChildren().add(new Text(tempPhoto.getCaption()));
				listItem.setAlignment(Pos.CENTER_LEFT);
				listItem.setSpacing(10);
				list.getItems().add(listItem);
			}
			//System.out.println("Exited loading loop");
		} catch (ClassNotFoundException e) {
			//System.out.println("Caught exception in fillAlbum()");
			//e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		try {
			//System.out.println("Finished loading album");
			inflow.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

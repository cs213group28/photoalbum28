/**
 * @author Manan, Mehul
 */
package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import model.Album;
import model.Photo;
import model.User;
import view.PhotoAlbum;

public class SearchController {
	@FXML ListView<HBox> list;
	@FXML TextField name; // Album name
	
	// Buttons
	@FXML Button save;
	
	
	int listIndex = 0; // Track where in the list we are
	ArrayList<Photo> photoList;

	
	
	/**
	 * 
	 * @param e - something from the list was chosen
	 */
	public void listChoice(MouseEvent e) {
		listIndex = list.getSelectionModel().getSelectedIndex();
		if(listIndex < 0){ // empty list
			return;
		}
	}
	
	/**
	 * Save results as their own album
	 * @param e - save pressed
	 */
	public void save(ActionEvent e){
		if(name.getText() == null || name.getText().length() <= 0){
			return;
		}
		
		// Otherwise create a new album with that name, and save it
		String filename;
		ObjectOutputStream out;
		File albumFile;

		filename = new String(PhotoAlbum.currentFolder.getPath());
		filename = filename.concat("\\");
		filename = filename.concat(name.getText());
		filename = filename.concat(".txt");
		
		albumFile = new File(filename);
		if(albumFile.exists()){
			albumFile.delete();
		}
		try {
			out = new ObjectOutputStream(new FileOutputStream(filename));
			out.writeObject((int)0);
			for(int x=0; x<photoList.size(); x++){
				out.writeObject((Photo)photoList.get(x));
			}
			out.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		return;
	}
	
	public void loadList(ArrayList<Photo> photos){
		photoList = photos;
		loadPhotos();
	}
	
	/**
	 * Load photos
	 */
	public void loadPhotos() {
		String filename;
		File albumFile;
		Photo tempPhoto;
		ObjectInputStream inflow;
		ImageView thumbnail;
		HBox listItem;
		String path = PhotoAlbum.currentFolder.getPath();
		
		if(list!=null)
			list.getItems().clear();
		
		
		try {
			for(int x=0; x<photoList.size(); x++){
				thumbnail = new ImageView(new Image(new FileInputStream(photoList.get(x).getAddress())));
				thumbnail.setFitWidth(100);
				thumbnail.setPreserveRatio(true);
				
				listItem = new HBox();
				listItem.getChildren().add(thumbnail);
				listItem.getChildren().add(new Text(photoList.get(x).getCaption()));
				listItem.setAlignment(Pos.CENTER_LEFT);
				listItem.setSpacing(10);
				list.getItems().add(listItem);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println("Loaded search results");
	}

}
